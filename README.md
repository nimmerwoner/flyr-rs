# Flyr
![](https://bitbucket.org/nimmerwoner/flyr-rs/downloads/flyr-cropped.png)

Flyr is a library for extracting thermal data from FLIR images written fully in
Rust. Files can be read with a single function call returning a 2D array with
the temperatures in Kelvin. This project has a sibling project in [`flyr-py`](https://bitbucket.org/nimmerwoner/flyr/src/master/),
which is Flyr fully written in Python.

## Installation
This library is available on [crates.io](https://crates.io/crates/flyr). Install
by adding `flyr = "0.4.0"` to your Cargo.toml.

## Usage
Call `try_parse_flir` on a filepath to extract the thermal data:

```rust
use flyr::try_parse_flir;

fn main() {
    // Return value is of type Result<Array<f32, Ix2> std::io::Error>
    let file_path = Path::new("/home/user/FLIR0923.jpg");
    let r_kelvin = try_parse_flir(file_path);
}
```

The array structure is provided by [https://crates.io/crates/ndarray](https://crates.io/crates/ndarray).

## Status
Currently this library has been tested to work with:

* FLIR C2
* FLIR E4
* FLIR E5
* FLIR E6
* FLIR E8
* FLIR E8XT
* FLIR E53
* FLIR E75
* FLIR T630SC
* FLIR T660

Camera's found not to work (yet):

* FLIR E60BX
* FLIR ThermoCAM B400
* FLIR ThermaCAM SC640
* FLIR ThermaCam SC660 WES
* FLIR ThermaCAM T-400
* FLIR S60 NTSC
* FLIR SC620 Western
* FLIR T400 (Western)
* FLIR T640
* FLIR P660

## Issue tracking
Issue tracking happens in the [Blackbody repository](https://bitbucket.org/nimmerwoner/blackbody/issues/).
