0.6.0 (unreleased)
------------------
* Added support for compiling to wasm
* Added support for rotating the data according to the EXIF rotation value

0.5.0 (08-07-2022)
------------------
* Added support for reading embedded optical imagery
* Added support for reading embedded thermal palettes
* Introduced FlyrThermogram object as main interface
* Added methods to retrieve temperatures in kelvin, celsius and fahrenheit
