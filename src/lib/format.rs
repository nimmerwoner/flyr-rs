use binread::*;
use std::convert::TryInto;

use crate::error::*;
use crate::orientation::calc_exif_corrected_indices;

#[derive(Clone, Debug, BinRead)]
#[br(magic = b"\xff\xe1", assert(&magic_flir == b"FLIR\x00"))]
pub struct FlirApp1Chunk {
    #[br(assert(length >= 10))]
    pub length: u16,
    pub magic_flir: [u8; 5],
    pub skip_byte: u8,
    pub chunk_idx: u8,
    pub num_chunks: u8,
    #[br(big, count = length - 10)]
    pub data: Vec<u8>,
}

#[derive(Clone, Debug, BinRead)]
#[br(magic = b"FFF\0")]
pub struct FlirRecord {
    pub creator: [u8; 16],
    pub file_format_version: u32,
    pub offset_record: u32,
    pub num_record_entries: u32,
    pub next_free_idx: u32,
    pub swap_pattern: u16,
    pub spares: [u16; 7],
    pub reserved: [u32; 2],
    pub checksum: u32,
}

#[derive(Clone, Debug, BinRead)]
pub struct FlirRecordEntryMetadata {
    pub record_type: u16,
    pub record_subtype: u16,
    pub record_version: u32,
    pub index_id: u32,
    pub offset: u32,
    pub length: u32,
    pub parent: u32,
    pub object_number: u32,
    pub checksum: u32,
}

#[derive(Clone, Debug, BinRead)]
pub struct FlirRawData {
    #[br(pad_before = 2)]
    #[br(little)]
    pub width: u16,
    #[br(little)]
    pub height: u16,
    pub type_: u16,
    #[br(pad_before = 24)]
    #[br(parse_with = until_eof)]
    pub bytes: Vec<u8>,
}

#[allow(dead_code)]
#[derive(Clone, Debug, BinRead)]
#[br(little)]
pub struct FlirEmbeddedImage {
    #[br(pad_before = 2)]
    #[br(little)]
    pub width: u16,
    #[br(little)]
    pub height: u16,
    pub type_: u16,
    #[br(pad_before = 24)]
    #[br(parse_with = until_eof)]
    pub bytes: Vec<u8>,
}

#[derive(Clone, Debug, BinRead)]
#[br(little)]
pub struct FlirCameraInfo {
    #[br(pad_before = 32)]
    pub emissivity: f32,
    pub object_distance: f32,
    pub reflected_apparant_temperature: f32,
    pub atmospheric_temperature: f32,
    pub ir_window_temperature: f32,
    pub ir_window_transmission: f32,
    #[br(pad_before = 4)]
    pub relative_humidity: f32,
    #[br(pad_before = 24)]
    pub planck_r1: f32,
    pub planck_b: f32,
    pub planck_f: f32,
    #[br(pad_before = 12)]
    pub atmospheric_trans_alpha1: f32,
    pub atmospheric_trans_alpha2: f32,
    pub atmospheric_trans_beta1: f32,
    pub atmospheric_trans_beta2: f32,
    pub atmospheric_trans_x: f32,
    #[br(pad_before = 644)]
    pub planck_o: i32,
    pub planck_r2: f32,
    #[br(pad_before = 40)]
    pub raw_value_median: u32,
    pub raw_value_range: u32,
}

#[derive(Clone, Debug, BinRead)]
pub struct FlirPaletteInfo {
    pub num_colors: u8,
    #[br(pad_before = 5)]
    pub above_color: [u8; 3],
    pub below_color: [u8; 3],
    pub overflow_color: [u8; 3],
    pub underflow_color: [u8; 3],
    pub isotherm1_color: [u8; 3],
    pub isotherm2_color: [u8; 3],
    #[br(pad_before = 2)]
    pub palette_method: u8,
    pub palette_stretch: u8,
    // skipping palette name and file name
    #[br(pad_before = 84)]
    #[br(little, count = num_colors)]
    pub palette: Vec<[u8; 3]>,
}

impl FlirPaletteInfo {
    pub fn palette_rgba(&self) -> Vec<[u8; 4]> {
        return self
            .palette
            .iter()
            // .rev()
            .map(FlirPaletteInfo::ycc_to_rgba)
            .collect();
    }

    fn ycc_to_rgba(ycc: &[u8; 3]) -> [u8; 4] {
        let y = ycc[0] as f32;
        let cr = ycc[1] as f32;
        let cb = ycc[2] as f32;

        let r = (298.082 * y / 256.0) + (408.583 * cr / 256.0) - 222.921;
        let g = (298.082 * y / 256.0)
            - (100.291 * cb / 256.0)
            - (208.120 * cr / 256.0)
            + 135.576;
        let b = (298.082 * y / 256.0) + (516.412 * cb / 256.0) - 276.836;

        let r = f32::round(r).min(255.0).max(0.0) as u8;
        let g = f32::round(g).min(255.0).max(0.0) as u8;
        let b = f32::round(b).min(255.0).max(0.0) as u8;

        return [r, g, b, 255];
    }
}

impl FlirRawData {
    pub fn kelvin(
        &self,
        raw: &[u16],
        info: &FlirCameraInfo,
        exif: &Option<u8>,
    ) -> Vec<f32> {
        let width: usize = self.width as usize;
        let height: usize = self.height as usize;
        let exif: u8 = exif.unwrap_or(1);
        let indices = calc_exif_corrected_indices(&width, &height, &exif);
        let data = self.kelvin_(raw, info);
        return indices
            .iter()
            .map(move |idx| unsafe { *data.get_unchecked(*idx) })
            .collect();
    }

    #[inline]
    pub fn kelvin_(
        &self,
        raw: &[u16],
        info: &FlirCameraInfo,
    ) -> Vec<f32> {
        return Self::raw2kelvin(raw, info);
    }

    pub fn read(&self) -> Result<Vec<u16>, io::Error> {
        let length = self.width as usize * self.height as usize;
        if self.bytes.len() > 4 && &self.bytes.as_slice()[0..4] == b"\x89PNG" {
            return image::load_from_memory(self.bytes.as_slice())
                .map_err(|err| invalid_data_err(&err.to_string()))
                .and_then(|img|
                    // Take the raw image values and put them in an array
                    // Then put raw values in right endian format
                    img.as_flat_samples_u16()
                    .ok_or(invalid_data_err("Failed parsing PNG image as u16 array"))
                    .map(|vals| vals.as_slice()
                            .iter()
                            .map(|val| (val >> 8) + ((val & 0x00FF) << 8))
                            .collect()
                    ));
        } else if self.bytes.len() / 2 == length && self.bytes.len() % 2 == 0 {
            let err = "Failed parsing raw data bytes as u16 array";
            let convert = |chunk: &[u8]| {
                u16::from_le_bytes(chunk.try_into().expect(err))
            };
            let s = self.bytes.chunks_exact(2).map(convert);
            return Ok(s.collect());
        } else {
            return Err(invalid_data_err("Failed parsing raw thermal data"));
        }
    }

    pub fn raw2kelvin(raw: &[u16], info: &FlirCameraInfo) -> Vec<f32> {
        // Transmission through window (calibrated)
        let emiss_wind = 1.0 - info.ir_window_transmission;
        let refl_wind = 0.0;

        // Transmission through the air
        let water = info.relative_humidity
            * std::f32::consts::E.powf(
                1.5587 + 0.06939 * (info.atmospheric_temperature - 273.15)
                    - 0.00027816
                        * (info.atmospheric_temperature - 273.15).powf(2.0)
                    + 0.00000068455
                        * (info.atmospheric_temperature - 273.15).powf(3.0),
            );

        let calc_atmos = |alpha: f32, beta: f32| -> f32 {
            let term1 = -(info.object_distance / 2.0).sqrt();
            let term2 = alpha + beta * water.sqrt();
            std::f32::consts::E.powf(term1 * term2)
        };

        let atmos1 = calc_atmos(
            info.atmospheric_trans_alpha1,
            info.atmospheric_trans_beta1,
        );
        let atmos2 = calc_atmos(
            info.atmospheric_trans_alpha2,
            info.atmospheric_trans_beta2,
        );
        let tau1 = info.atmospheric_trans_x * atmos1
            + (1.0 - info.atmospheric_trans_x) * atmos2;
        let tau2 = info.atmospheric_trans_x * atmos1
            + (1.0 - info.atmospheric_trans_x) * atmos2;

        // Radiance from the environment
        let plancked = |t: f32| -> f32 {
            let planck_tmp = info.planck_r2
                * (std::f32::consts::E.powf(info.planck_b / t)
                    - info.planck_f);
            info.planck_r1 / planck_tmp - (info.planck_o as f32)
        };

        let raw_refl1 = plancked(info.reflected_apparant_temperature);
        let raw_refl1_attn =
            (1.0 - info.emissivity) / info.emissivity * raw_refl1;

        let raw_atm1 = plancked(info.atmospheric_temperature);
        let raw_atm1_attn = (1.0 - tau1) / info.emissivity / tau1 * raw_atm1;

        let term3 = info.emissivity * tau1 * info.ir_window_transmission;
        let raw_wind = plancked(info.ir_window_temperature);
        let raw_wind_attn = emiss_wind / term3 * raw_wind;

        let raw_refl2 = plancked(info.reflected_apparant_temperature);
        let raw_refl2_attn = refl_wind / term3 * raw_refl2;

        let raw_atm2 = plancked(info.atmospheric_temperature);
        let raw_atm2_attn = (1.0 - tau2) / term3 / tau2 * raw_atm2;

        let subtraction = raw_atm1_attn
            + raw_atm2_attn
            + raw_wind_attn
            + raw_refl1_attn
            + raw_refl2_attn;

        return raw
            .iter()
            .map(|v| {
                let v = *v as f32;
                let v = v / (info.emissivity
                    * tau1
                    * info.ir_window_transmission
                    * tau2);
                let v = v - subtraction;

                // Temperature from radiance
                let v = v + info.planck_o as f32;
                let v = v * info.planck_r2;
                let v = info.planck_r1 / v + info.planck_f;
                let v = info.planck_b / v.ln();
                return v;
            })
            .collect();
    }
}

impl FlirEmbeddedImage {
    pub fn optical(&self, exif: Option<u8>) -> Result<Vec<u8>, io::Error> {
        let width = self.width as usize;
        let height = self.height as usize;
        let exif = exif.unwrap_or(1);
        let indices = calc_exif_corrected_indices(&width, &height, &exif);

        return self.optical_().map(|data| {
            indices
                .iter()
                .map(|idx| idx * 3)
                .flat_map(move |idx| unsafe {
                    vec![
                        *data.get_unchecked(idx),
                        *data.get_unchecked(idx + 1),
                        *data.get_unchecked(idx + 2),
                    ]
                })
                .collect()
        });
    }

    #[inline]
    pub fn optical_(&self) -> Result<Vec<u8>, io::Error> {
        // Err(invalid_data_err("Embedded image is not a valid image"))
        image::load_from_memory(self.bytes.as_slice())
            .ok()
            .and_then(|img| {
                img.as_flat_samples_u8()
                    .map(|vals| vals.as_slice().iter().map(|v| *v).collect())
            })
            .ok_or(invalid_data_err("Embedded image is not a valid image"))
    }
}

// File format references:
// * http://vip.sugovica.hu/Sardi/kepnezo/JPEG%20File%20Layout%20and%20Format.htm
// * https://en.wikipedia.org/wiki/JPEG#Syntax_and_structure
// * http://gvsoft.no-ip.org/exif/exif-explanation.html
// * https://dev.exiv2.org/projects/exiv2/wiki/The_Metadata_in_JPEG_files
// * https://rdrr.io/cran/Thermimage/man/readflirJPG.html
// * https://exiftool.org/TagNames/FLIR.html
// * https://github.com/kamadak/exif-rs https://docs.rs/kamadak-exif/0.5.1/exif/
// * https://crates.io/crates/implex
// * https://github.com/vadixidav/exifsd https://docs.rs/exifsd/0.1.0/exifsd/
