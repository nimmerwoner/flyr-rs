use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::{Clamped, JsCast};
use web_sys;

use crate::bindings::wasm::FlyrThermogramJs;

#[wasm_bindgen]
#[allow(non_snake_case)]
impl FlyrThermogramJs {
    pub fn renderToCanvas(
        &self,
        min_val: f32,
        max_val: f32,
        canvas_id: &str,
    ) -> Result<(), String> {
        // TODO Fix unwraps
        let render = self.render(min_val, max_val)?;

        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id(canvas_id).unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .map_err(|_| ())
            .unwrap();
        canvas.set_height(self.height() as u32);
        canvas.set_width(self.width() as u32);

        let context: web_sys::CanvasRenderingContext2d = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();

        let data = web_sys::ImageData::new_with_u8_clamped_array_and_sh(
            Clamped(&render),
            self.width() as u32,
            self.height() as u32,
        )
        .unwrap();
        context.put_image_data(&data, 0.0, 0.0).unwrap();

        return Ok(());
    }

    // pub fn drawOpticalToCanvas(&self, canvas_id: &str) -> Result<(), String> {
           // TODO Fix unwraps
    //     let optical = self.optical()?; // TODO Add alpha channel: 255 after rgb
    //     let height = self.optical_height().unwrap() as u32;
    //     let width = self.optical_height().unwrap() as u32;

    //     let document = web_sys::window().unwrap().document().unwrap();
    //     let canvas = document.get_element_by_id(canvas_id).unwrap();
    //     let canvas: web_sys::HtmlCanvasElement = canvas
    //         .dyn_into::<web_sys::HtmlCanvasElement>()
    //         .map_err(|_| ())
    //         .unwrap();
    //     canvas.set_height(height);
    //     canvas.set_width(width);

    //     let context: web_sys::CanvasRenderingContext2d = canvas
    //         .get_context("2d")
    //         .unwrap()
    //         .unwrap()
    //         .dyn_into::<web_sys::CanvasRenderingContext2d>()
    //         .unwrap();

    //     let data = web_sys::ImageData::new_with_u8_clamped_array_and_sh(
    //         Clamped(&optical),
    //         width,
    //         height,
    //     )
    //     .unwrap();
    //     context.put_image_data(&data, 0.0, 0.0).unwrap();

    //     return Ok(());
    // }
}
