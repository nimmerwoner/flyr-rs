use wasm_bindgen::prelude::wasm_bindgen;

use crate::thermogram::FlyrThermogram;

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct FlyrThermogramJs {
    thermogram: FlyrThermogram,
}

#[wasm_bindgen]
#[allow(non_snake_case)]
impl FlyrThermogramJs {
    pub fn newFromBytes(bytes: &[u8]) -> Result<FlyrThermogramJs, String> {
        let t = FlyrThermogram::new_from_bytes(bytes)
            .map_err(|e| e.to_string())?;
        return Ok(FlyrThermogramJs { thermogram: t });
    }

    pub fn width(&self) -> usize {
        return self.thermogram.width();
    }

    pub fn height(&self) -> usize {
        return self.thermogram.height();
    }

    pub fn orientation(&self) -> Option<u8> {
        return self.thermogram.orientation();
    }

    pub fn setOrientation(&mut self, orientation: Option<u8>) {
        return self.thermogram.set_orientation(orientation);
    }

    pub fn kelvin(&self) -> Vec<f32> {
        return self.thermogram.kelvin();
    }

    pub fn kelvin_(&self) -> Vec<f32> {
        return self.thermogram.kelvin_();
    }

    pub fn celsius(&self) -> Vec<f32> {
        return self.thermogram.celsius();
    }

    pub fn fahrenheit(&self) -> Vec<f32> {
        return self.thermogram.fahrenheit();
    }

    pub fn optical(&self) -> Result<Vec<u8>, String> {
        return self.thermogram.optical().map_err(|e| e.to_string());
    }

    pub fn optical_width(&self) -> Option<u16> {
        return self.thermogram.optical_width().map(|v| *v);
    }

    pub fn optical_height(&self) -> Option<u16> {
        return self.thermogram.optical_height().map(|v| *v);
    }

    pub fn render(
        &self,
        min_val: f32,
        max_val: f32,
    ) -> Result<Vec<u8>, String> {
        return self
            .thermogram
            .render(&min_val, &max_val)
            .map_err(|e| e.to_string());
    }

    pub fn defaultRenderRange(&self) -> Vec<f32> {
        return self
            .thermogram
            .default_render_range()
            .iter()
            .map(|v| *v)
            .collect();
    }
}
