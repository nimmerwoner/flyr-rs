use std::ffi::OsStr;
use std::io;
use std::path::Path;

use crate::error::invalid_data_err;
use crate::format::*;
use crate::orientation::extract_orientation;
use crate::parsing::read_flir_jpeg_stream;

#[derive(Clone, Debug)]
pub struct FlyrThermogram {
    pub raw_data: FlirRawData,
    pub raw_data_read: Vec<u16>,
    pub camera_info: FlirCameraInfo,
    pub palette_info: Option<FlirPaletteInfo>,
    pub embedded_image: Option<FlirEmbeddedImage>,
    pub orientation: Option<u8>,
}

impl FlyrThermogram {
    /// Directly instantiate a `FlyrThermogram` from parsed FLIR entities.
    ///
    /// Normally this is not what you would like to use. Instead, try
    /// `new_from_string` or `new_from_path`.
    ///
    /// # Returns
    /// A `FlyrThermogram` instance, otherwise an error of type `std::io::Error`.
    pub fn new(
        raw_data: FlirRawData,
        raw_data_read: Vec<u16>,
        camera_info: FlirCameraInfo,
        palette_info: Option<FlirPaletteInfo>,
        embedded_image: Option<FlirEmbeddedImage>,
        orientation: Option<u8>,
    ) -> FlyrThermogram {
        return FlyrThermogram {
            raw_data: raw_data,
            raw_data_read: raw_data_read,
            camera_info: camera_info,
            palette_info: palette_info,
            embedded_image: embedded_image,
            orientation: orientation,
        };
    }

    /// Tries to read a FLIR file by using a string reference to it.
    ///
    /// # Arguments
    /// * `file_path` - A path to the file to open
    ///
    /// # Returns
    /// A `FlyrThermogram` instance, otherwise an error of type `std::io::Error`.
    ///
    /// # Examples
    /// ```rust
    /// use flyr::thermogram::FlyrThermogram;
    ///
    /// fn main() {
    ///     let thermogram = FlyrThermogram::new_from_string("/home/user/FLIR0923.jpg");
    /// }
    /// ```
    pub fn new_from_string<S: AsRef<OsStr> + ?Sized>(
        file_path: &S,
    ) -> Result<FlyrThermogram, io::Error> {
        return FlyrThermogram::new_from_path(Path::new(file_path));
    }

    /// Tries to read a FLIR file by using a Path reference to it.
    ///
    /// # Arguments
    /// * `file_path` - A path to the file to open
    ///
    /// # Returns
    /// A `FlyrThermogram` instance, otherwise an error of type `std::io::Error`.
    ///
    /// # Examples
    /// ```rust
    /// use flyr::thermogram::FlyrThermogram;
    /// use std::path::Path;
    ///
    /// fn main() {
    ///     let file_path = Path::new("/home/user/FLIR0923.jpg");
    ///     let thermogram = FlyrThermogram::new_from_path(file_path);
    /// }
    /// ```
    pub fn new_from_path(
        file_path: &Path,
    ) -> Result<FlyrThermogram, io::Error> {
        let bytes = std::fs::read(file_path)?;
        return FlyrThermogram::new_from_bytes(bytes.as_slice());
    }

    /// Tries to read a FLIR file directly from a byte array.
    ///
    /// # Arguments
    /// * `bytes` - The byte array to parse.
    ///
    /// # Returns
    /// A `FlyrThermogram` instance, otherwise an error of type `std::io::Error`.
    ///
    /// # Examples
    /// ```rust
    /// use flyr::thermogram::FlyrThermogram;
    /// use std::fs;
    ///
    /// fn main() {
    ///     let bytes = fs::read("thermograms/flir_c5_1.jpg").unwrap();
    ///     let thermogram = FlyrThermogram::new_from_bytes(bytes.as_slice());
    /// }
    /// ```
    pub fn new_from_bytes(bytes: &[u8]) -> Result<FlyrThermogram, io::Error> {
        let orientation = extract_orientation(bytes);
        let mut t = read_flir_jpeg_stream(bytes)?;
        t.set_orientation(orientation);
        return Ok(t);
    }
}

impl FlyrThermogram {
    /// Returns the thermogram's width, corrected by the EXIF orientation if available.
    pub fn width(&self) -> usize {
        let orient = self.orientation.unwrap_or(1);
        let swap = (5..=8).contains(&orient);
        let width = match swap {
            true => self.raw_data.height,
            false => self.raw_data.width,
        };
        return width as usize;
    }

    /// Returns the thermogram's height, corrected by the EXIF orientation if available.
    pub fn height(&self) -> usize {
        let orient = self.orientation.unwrap_or(1);
        let swap = (5..=8).contains(&orient);
        let height = match swap {
            true => self.raw_data.width,
            false => self.raw_data.height,
        };
        return height as usize;
    }

    /// Returns the thermogram's EXIF orientation, if embedded, otherwise `None`.
    ///
    /// # Returns
    /// The EXIF value wrapped in `Some(..)` if present, otherwise None. The value is
    /// supposed to be between 1 and 8 (inclusive) but this is not checked.
    pub fn orientation(&self) -> Option<u8> {
        return self.orientation;
    }

    /// Sets the EXIF orientation value to use.
    ///
    /// # Arguments
    /// * `orientation` - The EXIF orientation to use wrapped in `Option`. The value is supposed
    ///     to be between 1 and 8 (inclusive), but this is not checked. `None` unsets the
    ///     orientation after which operations assume an EXIF orientation of 1.
    pub fn set_orientation(&mut self, orientation: Option<u8>) {
        self.orientation = orientation;
    }

    /// Returns the temperatures in kelvin, orientation corrected if available.
    pub fn kelvin(&self) -> Vec<f32> {
        return self.raw_data.kelvin(&self.raw_data_read, &self.camera_info, &self.orientation);
    }

    /// Returns the temperatures in kelvin without orientation correction.
    pub fn kelvin_(&self) -> Vec<f32> {
        return self.raw_data.kelvin_(&self.raw_data_read, &self.camera_info);
    }

    /// Returns the temperatures in celsius, orientation corrected if available.
    pub fn celsius(&self) -> Vec<f32> {
        let kelvin = self.kelvin();
        return kelvin.iter().map(|k| k - 273.15).collect();
    }

    /// Returns the temperatures in fahrenheit, orientation corrected if available.
    pub fn fahrenheit(&self) -> Vec<f32> {
        let kelvin = self.kelvin();
        return kelvin.iter().map(|k| k * 9.0 / 5.0 + 459.67).collect();
    }

    /// Returns the optical image if present without doing exif correction.
    ///
    /// # Returns
    /// A vector R, G and B values (u8 each) if a normal camera image is present. Otherwise an
    /// error. An error can also occur if the embedded image is corrupt.
    pub fn optical(&self) -> Result<Vec<u8>, io::Error> {
        match &self.embedded_image {
            None => Err(invalid_data_err("No optical")),
            Some(ei) => ei.optical(self.orientation),
        }
    }

    pub fn optical_width(&self) -> Option<&u16> {
        return self.embedded_image.as_ref().map(|ei| &ei.width);
    }

    pub fn optical_height(&self) -> Option<&u16> {
        return self.embedded_image.as_ref().map(|ei| &ei.width);
    }

    /// Renders the thermogram to an RGBA array.
    ///
    /// # Arguments
    /// * `min_val` - Kelvin. Clip all temperatures lower than this temperature to this value.
    /// * `max_val` - Kelvin. Clip all temperatures higher than this temperature to this value.
    ///
    /// # Returns
    /// An RGBA array (`Vec<u8>`) wrapped in a `Some` if succesful. The range of temperatures
    /// min_val .. max_val are linearly mapped to the embedded palette. In case of failure an
    /// `std::io::Error` is returned. This can happen due to no palette being embedded.
    pub fn render(
        &self,
        min_val: &f32,
        max_val: &f32,
    ) -> Result<Vec<u8>, io::Error> {
        let palette_info = self
            .palette_info
            .clone()
            .ok_or_else(|| invalid_data_err("No palette"))?; // TODO Default palette
        let palette = palette_info.palette_rgba();
        let num_bins = palette_info.palette.len();
        let max_bin = (num_bins - 1) as f32;

        let max_val = max_val - min_val;
        let norm = |v: &f32| (v - min_val).min(max_val).max(0.0) / max_val;

        let kelvin = self.kelvin();
        let render = kelvin
            .iter()
            .map(|k: &f32| {
                let bin = f32::round(norm(k) * max_bin) as usize;
                unsafe { *palette.get_unchecked(bin) }
            })
            .flatten()
            .collect();
        return Ok(render);
    }

    pub fn default_render_range(&self) -> [f32; 2] {
        let dev = self.camera_info.raw_value_range / 2;
        let lower = self.camera_info.raw_value_median - dev;
        let upper = self.camera_info.raw_value_median + dev;
        let extremes = [lower as u16, upper as u16];
        let extremes = FlirRawData::raw2kelvin(&extremes, &self.camera_info);
        return [extremes[0], extremes[1]];
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::*;
    use std::path::PathBuf;

    #[rstest]
    #[case::flir_c5_1("flir_c5_1.jpg", 160, 120)]
    #[case::flir_e4_1("flir_e4_1.jpg", 80, 60)]
    #[case::flir_e4_2("flir_e4_2.jpg", 320, 240)]
    #[case::flir_e4_3("flir_e4_3.jpg", 320, 240)]
    #[case::flir_e5_1("flir_e5_1.jpg", 120, 90)]
    #[case::flir_e5_2("flir_e5_2.jpg", 120, 90)]
    #[case::flir_e5_2_pip("flir_e5_2-pip.jpg", 120, 90)]
    #[case::flir_e6_1("flir_e6_1.jpg", 160, 120)]
    #[case::flir_e6_2("flir_e6_2.jpg", 160, 120)]
    #[case::flir_e6_3("flir_e6_3.jpg", 160, 120)]
    #[case::flir_e8_wifi_1("flir_e8_wifi_1.jpg", 320, 240)]
    #[case::flir_e8_wifi_2("flir_e8_wifi_2.jpg", 320, 240)]
    #[case::flir_e8_wifi_3("flir_e8_wifi_3.jpg", 320, 240)]
    #[case::flir_e8xt_1("flir_e8xt_1.jpg", 320, 240)]
    #[case::flir_e8xt_2("flir_e8xt_2.jpg", 320, 240)]
    #[case::flir_e8xt_3("flir_e8xt_3.jpg", 320, 240)]
    #[case::flir_e30bx_1("flir_e30bx_1.jpg", 320, 240)]
    #[case::flir_e30_1("flir_e30_1.jpg", 320, 240)]
    #[case::flir_e40_1("flir_e40_1.jpg", 160, 120)]
    #[case::flir_e40_2("flir_e40_2.jpg", 320, 240)]
    #[case::flir_e40_3("flir_e40_3.jpg", 320, 240)]
    #[case::flir_e40_4("flir_e40_4.jpg", 160, 120)]
    #[case::flir_e50_1("flir_e50_1.jpg", 240, 180)]
    #[case::flir_e50_2("flir_e50_2.jpg", 240, 180)]
    #[case::flir_e50_3("flir_e50_3.jpg", 240, 180)]
    #[case::flir_e60_1("flir_e60_1.jpg", 320, 240)]
    #[case::flir_e60_2("flir_e60_2.jpg", 320, 240)]
    #[case::flir_e60_3("flir_e60_3.jpg", 320, 240)]
    #[case::flir_e50bx_1("flir_e50bx_1.jpg", 240, 180)]
    #[case::flir_e50bx_2("flir_e50bx_2.jpg", 240, 180)]
    #[case::flir_e50bx_3("flir_e50bx_3.jpg", 240, 180)]
    #[case::flir_e60bx_1("flir_e60bx_1.jpg", 320, 240)]
    #[case::flir_e60bx_2("flir_e60bx_2.jpg", 320, 240)]
    #[case::flir_e60bx_3("flir_e60bx_3.jpg", 320, 240)]
    #[case::flir_e75_1("flir_e75_1.jpg", 240, 320)]
    #[case::flir_e75_2("flir_e75_2.jpg", 320, 240)]
    #[case::flir_e75_3("flir_e75_3.jpg", 240, 320)]
    #[case::flir_i5_1("flir_i5_1.jpg", 80, 80)]
    #[case::flir_i5_2("flir_i5_2.jpg", 80, 80)]
    #[case::flir_i5_3("flir_i5_3.jpg", 80, 80)]
    #[case::flir_one_1("flir_one_1.jpg", 480, 640)]
    #[case::flir_one_g2_1("flir_one_g2_1.jpg", 240, 320)]
    #[case::flir_one_g2_2("flir_one_g2_2.jpg", 240, 320)]
    #[case::flir_one_g2_3("flir_one_g2_3.jpg", 240, 320)]
    #[case::flir_one_pro_1("flir_one_pro_1.jpg", 480, 640)]
    #[case::flir_one_pro_next_gen_1("flir_one_pro_next_gen_1.jpg", 480, 640)]
    #[case::flir_one_pro_next_gen_2("flir_one_pro_next_gen_2.jpg", 480, 640)]
    #[case::flir_sc660_1("flir_sc660_1.jpg", 640, 480)]
    #[case::flir_sc660_2("flir_sc660_2.jpg", 640, 480)]
    #[case::flir_t630sc_1("flir_t630sc_1.jpg", 640, 480)]
    #[case::flir_t630sc_2("flir_t630sc_2.jpg", 640, 480)]
    #[case::flir_t630sc_3("flir_t630sc_3.jpg", 480, 640)]
    #[case::flir_thermocam_b400_1("flir_thermocam_b400_1.jpg", 320, 240)]
    #[case::flir_thermocam_b400_2("flir_thermocam_b400_2.jpg", 320, 240)]
    #[case::flir_thermocam_b400_3("flir_thermocam_b400_3.jpg", 320, 240)]
    pub fn parse_thermogram(
        #[case] file: &str,
        #[case] width: usize,
        #[case] height: usize,
    ) {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("thermograms");
        path.push(file);

        let parsed = FlyrThermogram::new_from_path(&path).expect("");
        assert_eq!(parsed.width(), width);
        assert_eq!(parsed.height(), height);
        // assert_eq!(parsed.orientation(), None);

        let kelvin = parsed.kelvin_();
        let length = width as usize * height as usize;
        assert_eq!(kelvin.len(), length);
    }

    #[rstest]
    #[case::flir_e4_1("flir_e4_1.jpg")]
    pub fn calculate_kelvin(#[case] file: &str) {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("thermograms");
        path.push(file);
        let thermogram = FlyrThermogram::new_from_path(&path).expect("");

        // Check camera info values
        assert_eq!(thermogram.camera_info.atmospheric_temperature, 293.15);
        assert_eq!(thermogram.camera_info.atmospheric_trans_alpha1, 0.006569);
        assert_eq!(thermogram.camera_info.atmospheric_trans_alpha2, 0.012620);
        assert_eq!(thermogram.camera_info.atmospheric_trans_beta1, -0.002276);
        assert_eq!(thermogram.camera_info.atmospheric_trans_beta2, -0.006670);
        assert_eq!(thermogram.camera_info.atmospheric_trans_x, 1.9);
        assert_eq!(thermogram.camera_info.emissivity, 0.95);
        assert_eq!(thermogram.camera_info.ir_window_temperature, 293.15);
        assert_eq!(thermogram.camera_info.ir_window_transmission, 1.0);
        assert_eq!(thermogram.camera_info.object_distance, 1.0);
        assert_eq!(thermogram.camera_info.planck_b, 1330.2);
        assert_eq!(thermogram.camera_info.planck_f, 1.6);
        assert_eq!(thermogram.camera_info.planck_o, -6018);
        assert_eq!(thermogram.camera_info.planck_r1, 11857.259);
        assert_eq!(thermogram.camera_info.planck_r2, 0.02409865);
        assert_eq!(thermogram.camera_info.raw_value_median, 9600);  // FIXME
        assert_eq!(thermogram.camera_info.raw_value_range, 512);  // FIXME
        // assert_eq!(thermogram.camera_info.raw_value_range_min, 6980);
        // assert_eq!(thermogram.camera_info.raw_value_range_max, 57942);
        assert_eq!(thermogram.camera_info.reflected_apparant_temperature, 293.15);
        assert_eq!(thermogram.camera_info.relative_humidity, 0.5);
        let vals = &thermogram.raw_data_read;
        assert_eq!(vals.as_slice()[0..4], [9958, 9940, 9937, 9917]);

        let raw: [u16; 4] = [9958, 9940, 9937, 9917];
        let kelvin = FlirRawData::raw2kelvin(&raw, &thermogram.camera_info);
        assert_eq!(kelvin, [273.57816, 273.3021, 273.25598, 272.9481]);
    }

    // #[rstest]
    // #[should_panic]
    // pub fn parse_thermogram_fails(#[case] file: &str) {
    //     let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    //     path.push("thermograms");
    //     path.push(file);
    //     let parsed = FlyrThermogram::new_from_path(&path).expect("");
    //     let kelvin = parsed.kelvin_().expect("");

    //     let width = parsed.width();
    //     let height = parsed.height();
    //     let length = width as usize * height as usize;
    //     assert_eq!(kelvin.len(), length)
    // }
}
