use crate::error::invalid_data_err;
use crate::format::*;
use crate::thermogram::FlyrThermogram;

use binread::io::Read;
use binread::io::Seek;
use binread::*;

use std::collections::HashMap;
use std::io::Cursor;
use std::io::SeekFrom;

pub fn read_flir_jpeg_stream(
    bytes: &[u8],
) -> Result<FlyrThermogram, io::Error> {
    let app1 = extract_flir_app1(bytes)?;
    let record_directory = parse_record_directory(&app1.as_slice())?;
    parse_dir_entries(&app1.as_slice(), &record_directory)
}

fn extract_flir_app1(bytes: &[u8]) -> Result<Vec<u8>, io::Error> {
    let mut flir_app1_bytes = Vec::new();

    for (idx, byte) in bytes.into_iter().enumerate() {
        if byte != &b'\xff' {
            continue;
        }

        let mut c = Cursor::new(&bytes[idx..]);
        if let Ok(chunk) = c.read_be::<FlirApp1Chunk>() {
            flir_app1_bytes.extend(chunk.data);
        }
    }

    Ok(flir_app1_bytes)
}

fn parse_record_directory(
    bytes: &[u8],
) -> Result<HashMap<u16, FlirRecordEntryMetadata>, io::Error> {
    let mut c = Cursor::new(&bytes);
    let mut record_directory: HashMap<u16, FlirRecordEntryMetadata> =
        HashMap::new();
    while let Ok(record) = c.read_be::<FlirRecord>() {
        let mut cursor = Cursor::new(&bytes);
        cursor.seek(SeekFrom::Current(record.offset_record as i64))?;

        let capacity = 32usize * record.num_record_entries as usize;
        let mut dir_bytes_buf = vec![0u8; capacity];

        cursor.read(dir_bytes_buf.as_mut_slice())?;
        let mut dir_bytes = Cursor::new(&dir_bytes_buf);
        while let Ok(e_entry_md) =
            dir_bytes.read_be::<FlirRecordEntryMetadata>()
        {
            record_directory.insert(e_entry_md.record_type, e_entry_md);
        }
    }

    Ok(record_directory)
}

fn parse_dir_entries(
    bytes: &[u8],
    record_directory: &HashMap<u16, FlirRecordEntryMetadata>,
) -> Result<FlyrThermogram, io::Error> {
    let raw_data = record_directory
        .get(&1)
        .ok_or(invalid_data_err("No raw data present"))
        .map(|entry| parse_raw_data(bytes, entry))??;
    let raw_data_read = raw_data.read()?;
    let camera_info = record_directory
        .get(&32)
        .ok_or(invalid_data_err("No camera info present"))
        .map(|entry| parse_camera_info(bytes, entry))??;

    let pi = record_directory
        .get(&34)
        .map(|entry| parse_palette_info(bytes, entry).ok())
        .flatten();

    let ei: Option<FlirEmbeddedImage> = record_directory
        .get(&14)
        .map(|entry| parse_embedded_image(bytes, entry).ok())
        .flatten();

    let thermogram = FlyrThermogram::new(
        raw_data,
        raw_data_read,
        camera_info,
        pi,
        ei,
        None,
    );

    Ok(thermogram)
}

fn parse_raw_data(
    bytes: &[u8],
    metadata: &FlirRecordEntryMetadata,
) -> Result<FlirRawData, io::Error> {
    // Array<f32, Ix2>
    let start = metadata.offset as usize;
    let end = start + metadata.length as usize;
    let raw_data_bytes = &bytes[start..end]; // flir_app1_bytes

    // println!("RAW WH {:?}x{:?}  -->  Lengths: {:?} =? {:?} =? {:?}",
    //     raw_data.raw_thermal_image_width,
    //     raw_data.raw_thermal_image_height,
    //     raw_data.raw_thermal_image_width as u64 * raw_data.raw_thermal_image_height as u64,
    //     metadata.length,
    //     raw_data.raw_thermal_image.len(),
    // );
    // println!("IMG: {:?}", image::guess_format(raw_data.raw_thermal_image.as_slice()));
    Cursor::new(raw_data_bytes)
        .read_be::<FlirRawData>()
        .map_err(|_| invalid_data_err("Failed reading FLIR image's raw data"))
}

fn parse_embedded_image(
    bytes: &[u8],
    metadata: &FlirRecordEntryMetadata,
) -> Result<FlirEmbeddedImage, io::Error> {
    // Array<f32, Ix2>
    let start = metadata.offset as usize;
    let end = start + metadata.length as usize;
    let embedded_image_bytes = &bytes[start..end]; // flir_app1_bytes

    // println!("RAW WH {:?}x{:?}  -->  Lengths: {:?} =? {:?} =? {:?}",
    //     raw_data.raw_thermal_image_width,
    //     raw_data.raw_thermal_image_height,
    //     raw_data.raw_thermal_image_width as u64 * raw_data.raw_thermal_image_height as u64,
    //     metadata.length,
    //     raw_data.raw_thermal_image.len(),
    // );
    // println!("IMG: {:?}", image::guess_format(raw_data.raw_thermal_image.as_slice()));
    Cursor::new(embedded_image_bytes)
        .read_be::<FlirEmbeddedImage>()
        .map_err(|_| invalid_data_err("Failed reading FLIR image's raw data"))
}

fn parse_camera_info(
    bytes: &[u8],
    metadata: &FlirRecordEntryMetadata,
) -> Result<FlirCameraInfo, io::Error> {
    let start = metadata.offset as usize;
    let end = start + metadata.length as usize;
    let camera_info_bytes = &bytes[start..end];

    Cursor::new(camera_info_bytes)
        .read_be::<FlirCameraInfo>()
        .map_err(|_| {
            invalid_data_err("Failed reading FLIR image's camera info")
        })
}

fn parse_palette_info(
    bytes: &[u8],
    metadata: &FlirRecordEntryMetadata,
) -> Result<FlirPaletteInfo, io::Error> {
    let start = metadata.offset as usize;
    let end = start + metadata.length as usize;
    let palette_info_bytes = &bytes[start..end];

    Cursor::new(palette_info_bytes)
        .read_be::<FlirPaletteInfo>()
        .map_err(|_| invalid_data_err("Failed reading palette"))
}
