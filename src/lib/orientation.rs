use rexif::{ExifEntry, ExifTag};

pub fn calc_exif_corrected_indices(
    width: &usize,
    height: &usize,
    exif: &u8,
) -> Vec<usize> {
    // Note: Each value in the result vector is expected to be less than width * height

    // Ensure exif value lies between 1 and 8; set to 1 if outside
    let exif = if (1..=8).contains(exif) { exif } else { &1 };

    // Select function mapping (y, x) coords to pixel index
    let map_coords: Box<dyn Fn(&usize, &usize) -> usize> = match exif {
        1 => Box::new(|y: &usize, x: &usize| y * width + x),
        2 => Box::new(|y: &usize, x: &usize| y * width + (width - 1 - x)),
        3 => Box::new(|y: &usize, x: &usize| {
            (height - 1 - y) * width + (width - 1 - x)
        }),
        4 => Box::new(|y: &usize, x: &usize| (height - 1 - y) * width + x),
        5 => Box::new(|y: &usize, x: &usize| {
            (width * height) - 1 - x * width - y
        }),
        6 => Box::new(|y: &usize, x: &usize| y + width * x),
        7 => Box::new(|y: &usize, x: &usize| {
            (height - 1) * width + y - width * x
        }),
        8 => Box::new(|y: &usize, x: &usize| (width - 1 - y) + x * width),
        _ => Box::new(|y: &usize, x: &usize| y * width + x),
    };

    // Swap height / width for orientations putting the image on its side
    let swap = (5..=8).contains(exif);
    let (height, width) = if swap {
        (width, height)
    } else {
        (height, width)
    };

    // Calculate indices order that correctly rotates the data
    let coords = (0..*height).flat_map(|y| (0..*width).map(move |x| (y, x)));
    return coords.map(|(y, x)| map_coords(&y, &x)).collect();
}

pub fn extract_orientation(bytes: &[u8]) -> Option<u8> {
    return rexif::parse_buffer(bytes)
        // .map_err(|err| panic!(err.to_string()))
        .ok()
        .map(|exif| {
            exif.entries
                .iter()
                .reduce(|accum: &ExifEntry, item: &ExifEntry| {
                    if item.tag == ExifTag::Orientation {
                        item
                    } else {
                        accum
                    }
                })
                .map(|v| v.clone())
        })
        .flatten()
        .map(|item| match item.tag {
            ExifTag::Orientation => Some(item.value.to_i64(0)),
            _ => None,
        })
        .flatten()
        .flatten()
        .map(|value| value as u8);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn exif_rotation_0() {
        let result: [usize; 8] = [0, 1, 2, 3, 4, 5, 6, 7];
        let indices = calc_exif_corrected_indices(&2, &4, &0);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_1() {
        let result: [usize; 8] = [0, 1, 2, 3, 4, 5, 6, 7];
        let indices = calc_exif_corrected_indices(&2, &4, &1);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_2() {
        let result: [usize; 8] = [1, 0, 3, 2, 5, 4, 7, 6];
        let indices = calc_exif_corrected_indices(&2, &4, &2);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_3() {
        let result: [usize; 8] = [7, 6, 5, 4, 3, 2, 1, 0];
        let indices = calc_exif_corrected_indices(&2, &4, &3);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_4() {
        let result: [usize; 8] = [6, 7, 4, 5, 2, 3, 0, 1];
        let indices = calc_exif_corrected_indices(&2, &4, &4);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_5() {
        let result: [usize; 8] = [7, 5, 3, 1, 6, 4, 2, 0];
        let indices = calc_exif_corrected_indices(&2, &4, &5);
        println!("Indices 7: {:?}", indices);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_6() {
        let result: [usize; 8] = [0, 2, 4, 6, 1, 3, 5, 7];
        let indices = calc_exif_corrected_indices(&2, &4, &6);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_7() {
        let result: [usize; 8] = [6, 4, 2, 0, 7, 5, 3, 1];
        let indices = calc_exif_corrected_indices(&2, &4, &7);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }

    #[test]
    fn exif_rotation_8() {
        let result: [usize; 8] = [1, 3, 5, 7, 0, 2, 4, 6];
        let indices = calc_exif_corrected_indices(&2, &4, &8);
        let all_equal = result
            .iter()
            .zip(indices.iter())
            .map(|(i, j)| i == j)
            .fold(true, |acc, b| acc && b);
        assert!(all_equal);
    }
}
