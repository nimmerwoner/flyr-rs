//! [Flyr](https://bitbucket.org/nimmerwoner/flyr-rs/) is a library for extracting thermal data
//! from FLIR images written fully in Rust. Files can be read with a single function call
//! returning a 2D array with the temperatures in Kelvin.
//!
//! Flyr's abilities are showcased by [Blackbody](https://bitbucket.org/nimmerwoner/blackbody/src/b8dfcf7116abdeff1f0c986e9f0830a5b4d4e942/PREVIEW.md),
//! a thermogram viewer for Linux. Flyr itself is not tied to a specific OS. The project also has a
//! sibling project in [flyr-py](https://bitbucket.org/nimmerwoner/flyr/src/master/), which is
//! Flyr fully written in Python.
//!
//! # Installation
//! This library is available on [crates.io](https://crates.io/crates/flyr). Install
//! by adding `flyr = "0.4.0"` to your Cargo.toml. The source code can be found in
//! its [repository](https://bitbucket.org/nimmerwoner/flyr-rs/).
//!
//! # Features
//! * Reading many FLIR thermograms, both thermal and optical imagery
//! * Rendering with custom rendering options
//! * Compilable to wasm for usage within JavaScript
//!
//! # Usage
//! Most important functionality is accessible via the `FlyrThermogram` struct. This page
//! describes how to use the crate, however.
//!
//! ## Reading
//! `FlyrThermogram` can be instantiated from a file or directly from a slice of bytes.
//!
//! ```rust
//! // Read from a file
//! use flyr::thermogram::FlyrThermogram;
//! use std::path::Path;
//!
//! let file_path_str = "thermograms/flir_e40_4.jpg";
//! let thermogram: Result<FlyrThermogram, std::io::Error> = FlyrThermogram::new_from_string(file_path_str);
//!
//! let file_path = Path::new(file_path_str);
//! let thermogram = FlyrThermogram::new_from_path(file_path);
//! ```
//!
//! ```rust
//! // Alternatively, read from a slice of bytes
//! use flyr::thermogram::FlyrThermogram;
//! use std::path::Path;
//!
//! let file_path = Path::new("thermograms/flir_e40_4.jpg");
//! let bytes = std::fs::read(file_path).unwrap();
//! let thermogram = FlyrThermogram::new_from_bytes(bytes.as_slice());
//! ```
//!
//! ## Accessing temperature data in different units
//! ```rust
//! use flyr::thermogram::FlyrThermogram;
//! use std::path::Path;
//!
//! let file_path = Path::new("thermograms/flir_e40_4.jpg");
//! let thermogram = FlyrThermogram::new_from_path(file_path).unwrap();
//!
//! // Temperatures are available in various units
//! let kelvin: Vec<f32> = thermogram.kelvin();
//! let celsius: Vec<f32> = thermogram.celsius();
//! let fahrenheit: Vec<f32> = thermogram.fahrenheit();
//! ```
//!
//! ## Reading optical data
//! ```rust
//! use flyr::thermogram::FlyrThermogram;
//! use std::path::Path;
//!
//! let file_path = Path::new("thermograms/flir_e40_4.jpg");
//! let thermogram = FlyrThermogram::new_from_path(file_path).unwrap();
//!
//! let optical: Vec<u8> = thermogram.optical().unwrap();
//! ```
//!
//! ## Built-in support for rendering
//! ```rust
//! use flyr::thermogram::FlyrThermogram;
//! use std::path::Path;
//!
//! let file_path = Path::new("thermograms/flir_t630sc_1.jpg");
//! let thermogram = FlyrThermogram::new_from_path(file_path).unwrap();
//!
//! // Render as the FLIR camera originally did. Output is an RGBA array.
//! let min_max = thermogram.default_render_range();
//! let render: Vec<u8> = thermogram.render(&min_max[0], &min_max[1]).unwrap();
//!
//! // Render with custom min/max temperature range. Output is an RGBA array.
//! let (min, max) = (0.0, 20.0);
//! let render: Vec<u8> = thermogram.render(&min, &max).unwrap();
//! ```

//!
//! # Status
//! Currently this library has been tested to work with:
//!
//! * FLIR E4
//! * FLIR E6
//! * FLIR E8
//! * FLIR E8XT
//! * FLIR E53
//! * FLIR E75
//! * FLIR T630SC
//! * FLIR T660
//!
//! Camera's found not to work (yet):
//!
//! * FLIR E60BX
//! * FLIR ThermoCAM B400
//! * FLIR ThermaCAM SC640
//! * FLIR ThermaCam SC660 WES
//! * FLIR ThermaCAM T-400
//! * FLIR S60 NTSC
//! * FLIR SC620 Western
//! * FLIR T400 (Western)
//! * FLIR T640
//! * FLIR P660
//!

// #[cfg(target_family = "wasm")]
// #[global_allocator]
// static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub mod bindings;
mod error;
mod format;
mod orientation;
mod parsing;
pub mod thermogram;

// pub fn main() {}
